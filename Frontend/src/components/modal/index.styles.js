import styled from 'styled-components'

export const Container = styled.div`
  position: fixed;
  z-index: 1;
  background-color: rgba(0,0,0,0.2);
  width: 100%;
  height: 100%;

  display: none;
  justify-content: center;
  align-items: center;
  
  ${props => props.visible && `
    display: flex;
  `}
`
