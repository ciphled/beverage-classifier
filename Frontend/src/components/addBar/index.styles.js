import styled from 'styled-components'

export const Styles = styled.div`
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
    }

    /* Firefox */
    input[type=number] {
    -moz-appearance: textfield;
    }
    display: flex;
    align-items: center;
    height: 100%;
    input { 
        border-radius: 8px;
        width: 100%;
        height: 100%;
        border: none;
        &:focus {
            outline: none;
        }
        background: none;
        font-size: 17px;
    }
    padding-top: 10px;
`

export const AbsoluteWrapper = styled.div`
  position: absolute;
  height: 100%;
  width: 100%;
`

export const TitleContainer = styled.div`
  display: flex;
  height: 100%;
  justify-content: flex-start;
  align-items: center;
  transition: all 0.1s linear;
  ${(props) =>
    props.isUp &&
    `
        align-items: flex-start;
        font-size: smaller;
    `}
`

export const Container = styled.div`
  display: grid;
  grid-template-columns: 1fr auto;
  width: 100%;
  height: 40px;
  border-radius: 5px;
  border: 1px solid lightgrey;
  margin: 5px;
  padding: 8px;
  color: rgba(0, 0, 0, 0.6);
  ${(props) =>
    !props.isFocused &&
    `
    &:hover {
        border: 1px solid grey;
      }
    `}

  ${(props) =>
    props.isFocused &&
    `
    border: 1px solid rgba(0,150,255);
    color: rgba(0,150,255);
    `}
`

export const FieldContainer = styled.div`
  display: flex;
  position: relative;
`
