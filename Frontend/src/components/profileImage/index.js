import { Image } from './index.styles'

const placeholder = 'https://hancroft.co.nz/wp-content/uploads/2019/05/profile-placeholder.png'

function ProfileImage ({ onClick }) {
  const handleClick = () => {
    if (onClick) onClick()
  }
  return (
    <Image
      src={placeholder}
      onClick={handleClick}
      height='40'
      width='40'
    />
  )
}

export default ProfileImage
