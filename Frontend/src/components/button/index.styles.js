import styled from 'styled-components'
export const Container = styled.span`
  display: inline-grid;
  position: relative;
  grid-auto-flow: column;
  gap: 5px;
  align-items: center;
  justify-content: center;
  color: white;
  padding: 10px;
  border-radius: 5px;
  user-select: none;
  &:hover {
    opacity: 0.9;
  }
  cursor: pointer;
  &:active {
    opacity: 1;
  }
  ${(props) =>
    props.color &&
    `
  background: ${props.color};
`}
`

export const LinkContainer = styled.span`
  text-align: center;
  color: rgb(0, 100, 255);
  font-size: 15px;
  font-weight: 600;
  cursor: pointer; 
  padding: 3px;
  &:hover {
   opacity: 0.8;
  }
  cursor: pointer;
  &:active {
    background: rgba(0, 100, 255, 0.2);
  }
  border-radius: 3px;
`
