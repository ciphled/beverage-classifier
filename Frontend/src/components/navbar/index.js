import logo from './logo.png'
import { ProfileContainer, StyledNav } from './index.styles'
import ProfileImage from '../profileImage'
import Logout from '../logout'
import { supabase } from '../../lib/supabase'
import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

/* [Logo]   [Profile Image] [Logout] */
function handleLogout () {
  supabase.auth.signOut()
}

function Navbar (props) {
  const [loggedIn, setLoggedIn] = useState(!!supabase.auth.session())
  useEffect(() => {
    supabase.auth.onAuthStateChange((e, session) => {
      setLoggedIn(!!session)
    })
  }, [])

  return (
    <StyledNav>
      <Link to='/'>
        <img src={logo} height='40' alt='logo' />
      </Link>

      {loggedIn && (
        <ProfileContainer loggedIn={loggedIn}>
          <Link to='/profile'>
            <ProfileImage />
          </Link>
          <Logout onClick={handleLogout} />
        </ProfileContainer>
      )}
    </StyledNav>
  )
}

export default Navbar
