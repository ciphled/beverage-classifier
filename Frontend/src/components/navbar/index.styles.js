import { Link } from 'react-router-dom'
import styled from 'styled-components'

export const StyledNav = styled.nav`
    display: flex;
    justify-content: space-between;
    align-items: center;
    background: rgba(100,100,0,0.05);
    padding: 5px 10px;
    box-shadow: 0px 1px 4px rgba(0,0,0,0.2);
`

export const StyledLink = styled(Link)`

`

export const ProfileContainer = styled.div`
    display: grid; 
    grid-template-columns: auto auto;
    gap: 5px;
`
