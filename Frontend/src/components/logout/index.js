import { Title } from './index.styles'

function Logout ({ onClick }) {
  const handleClick = () => {
    if (onClick) onClick()
  }
  return <Title onClick={handleClick}>Logout</Title>
}

export default Logout
