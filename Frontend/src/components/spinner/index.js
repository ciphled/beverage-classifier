import styled, { keyframes } from 'styled-components'

const rotate360 = keyframes`
    from {
        transform: rotate(0deg);
    }
    to {
        transform: rotate(360deg);
    }
`

const Spinner = styled.div`
    animation: ${rotate360} 1s cubic-bezier(0.5, 0, 0.5, 1) infinite;
    border-top: 3px solid rgb(0,150,255,0.3);
    border-right: 3px solid rgb(0,150,255,0.3);
    border-bottom: 3px solid rgb(0,150,255,0.3);
    border-left: 3px solid rgb(0,150,255,0.9);
    width: 28px;
    height: 28px;
    ${props => props.size && `
        width: ${props.size};
        height: ${props.size};
    `}
    border-radius: 50%;
`

export default Spinner
