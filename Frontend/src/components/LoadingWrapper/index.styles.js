import styled from 'styled-components'

export const LoadingContainer = styled.div`
    display: flex;
    position: absolute;
    height: 100%;
    width: 100%;
    justify-content: center;
    align-items: center;
    background: rgba(255,255,255,0.75);
    z-index: 999;
    ${props => !props.isLoading && `
        display: none;
    `}
`
