import LoadingWrapper from '../LoadingWrapper'
import { Container, LinkContainer } from './index.styles'

function Button ({ onClick, Icon, children, color, variant, isLoading }) {
  const handleOnClick = () => {
    if (onClick) onClick()
  }
  if (variant === 'link') {
    return (
      <LinkContainer color={color || 'rgba(0, 100, 255, 1)'} onClick={handleOnClick}>
        {children}
        {Icon && <Icon />}
      </LinkContainer>
    )
  } else {
    return (
      <Container color={color || 'rgba(0, 100, 255, 1)'} onClick={handleOnClick}>
        <LoadingWrapper isLoading={isLoading} size='18px' />
        {children}
        {Icon && <Icon />}
      </Container>
    )
  }
}

export default Button
