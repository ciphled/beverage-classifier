import { useEffect, useRef, useState } from 'react'
import LoadingWrapper from '../LoadingWrapper'

import {
  AbsoluteWrapper,
  TitleContainer,
  Styles,
  Container,
  FieldContainer
} from './index.styles'

/*
number, text
*/
function Field ({ title, children, value, isLoading, onBlur, ...props }) {
  const [isUp, setIsUp] = useState()
  const [isFocused, setIsFocused] = useState()
  const ref = useRef()

  useEffect(() => {
    setIsUp(!!value)
  }, [value])

  const onFocus = () => {
    setIsUp(true)
    setIsFocused(true)
  }
  const handleOnBlur = (e) => {
    const value = ref.current.value
    const newIsUp = !!value
    setIsUp(newIsUp)
    if (!newIsUp && ref) ref.current.value = ''
    setIsFocused(false)
    if (onBlur) onBlur(e)
  }
  return (
    <Container isFocused={isFocused}>
      <LoadingWrapper isLoading={isLoading} />
      <FieldContainer>
        <AbsoluteWrapper>
          <TitleContainer isUp={isUp}>{title}</TitleContainer>
        </AbsoluteWrapper>
        <AbsoluteWrapper>
          <Styles>
            <input
              ref={ref}
              value={value}
              {...props}
              onFocus={onFocus}
              onBlur={handleOnBlur}
            />
          </Styles>
        </AbsoluteWrapper>
      </FieldContainer>
      {children}
    </Container>
  )
}

export default Field
