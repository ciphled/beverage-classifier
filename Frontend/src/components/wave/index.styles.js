import styled, { keyframes } from 'styled-components'

const waveAnimation = keyframes`
    0% {
        transform: scale(1,0);
    }
    100% {
        transform: scale(1,1);
    }
`

export const WaveContainer = styled.div`
    width: 100%;
    position: absolute;
    overflow: hidden;
    bottom: 0;
    background: #f3f4f5;
    z-index: -1;
    transform-origin: bottom;
    animation: ${waveAnimation} 1s cubic-bezier(0.2, 1, 0.3, 1) forwards;
`

export const Container = styled.div`
`
