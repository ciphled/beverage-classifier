import Home from './home'
import SignIn from './signin'
import SignUp from './signup'
import ResetPassword from './resetPassword'
import Profile from './profile'

export {
  Home,
  SignIn,
  SignUp,
  ResetPassword,
  Profile
}
