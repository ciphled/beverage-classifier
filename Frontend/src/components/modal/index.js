import React from 'react'
import ReactDOM from 'react-dom'
import { Container } from './index.styles'

function Modal ({ children, visible = false }) {
  const content = (
    <Container visible={visible}>
      {children}
    </Container>
  )
  return (
    ReactDOM.createPortal(content, document.getElementById('modal'))
  )
}

export default Modal
