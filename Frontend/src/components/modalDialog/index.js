import { useEffect, useState } from 'react'
import Button from '../button'
import Divider from '../divider'
import Modal from '../modal'
import { Container, ContentContainer, ButtonsContainer } from './index.styles'
function ModalDialog ({ text = 'Body', header = 'Header', onConfirm, onCancel, show }) {
  const [visible, setVisible] = useState(false)
  useEffect(() => {
    setVisible(show)
  }, [show])
  const handleConfirm = () => {
    if (onConfirm) {
      onConfirm()
    }
  }
  const handleCancel = () => {
    if (onCancel) {
      onCancel()
    }
  }
  return (
    <Modal visible={visible}>
      <Container>
        <ContentContainer>
          <h3>
            {header}
          </h3>
          {text}
        </ContentContainer>
        <Divider />
        <ButtonsContainer>
          <Button onClick={handleConfirm}>Confirm</Button>
          <Button color='grey' onClick={handleCancel}>Cancel</Button>
        </ButtonsContainer>
      </Container>
    </Modal>
  )
}

export default ModalDialog
