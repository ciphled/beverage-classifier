import styled from 'styled-components'

const width = 50
const size = 26

export const Container = styled.div`
  height: ${size}px;
  width: ${width}px;
  border-radius: ${size}px;
  position: relative;
  background: lightgrey;
  display: flex;
  align-items: center;
  padding: 3px;
  transition: all 0.15s linear;
  ${(props) =>
    props.isOn &&
    `
    background: ${props.color || '#28a745'};
    `}
`

export const Toggle = styled.div`
  position: relative;
  border-radius: 50%;
  width: ${size}px;
  height: ${size}px;
  background: white;
  transition: all 0.15s linear;

  ${(props) =>
    !props.isOn &&
    `
        transform: translateX(0);
    `}
  ${(props) =>
    props.isOn &&
    `
        transform: translateX(${width - size}px);
    `}
`
