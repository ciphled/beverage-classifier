<p align="center">
<img 
src="https://i.imgur.com/90sIVX3.png"
alt="logo"
width="120"
/>
<br>
<br>
Beverage Classifying Shopping List 🍻
<br>
<a href="https://bevlist.netlify.app" rel="noopener noreferrer">bevlist.netlify.app</a>
<br>
<br>
<a href="https://app.netlify.com/sites/bevlist/deploys" rel="noopener noreferrer">
    <img
    src="https://api.netlify.com/api/v1/badges/37961c5e-3c94-4dc2-aa34-3159524fe3ce/deploy-status"
    alt="Netlify Status"
    />
</a>
</p>

--- 

<br>

A beverage classifying list that classifies items as either beer, liquor, wine, kegs, dunnage or non-alcoholic using an NLP classifier trained on this <a href="https://www.kaggle.com/divyeshardeshana/warehouse-and-retail-sales" rel="noopener noreferrer">Kaggle dataset</a>, allowing for create, read, update and delete operations on the items. In addition the site features authentication & user profile management. Try it out for yourself over at <a href="https://bevlist.netlify.app" rel="noopener noreferrer">bevlist.netlify.app</a> (`u: winton.nathan.roberts@gmail.com, p: password`).
<br>
<br>

The project is intended to demonstrate some of my awareness and skills in the following:

 - Docker
 - React (and fundamental packages like Styled & Storybook)
 - AWS Lambda, Gateway API, CloudWatch
 - Keras/Tensorflow
 - Python
 - Netlify
 - (Postgres) SQL
 - BitBucket pipelines, and git more generally (CI/CD)
 - UX/UI
 
<br>

## Architecture

<img 
src="https://i.imgur.com/XrCr0ay.png"
style="max-width: 500px;"
/>

## Locally Test

**With docker:**
<br>
```
docker compose build
docker compose up
```

**Without docker:**
<br>
```
cd Frontend && yarn start
cd Backend && sls offline start
```

To view components in isolation, view the storybooks:
```
yarn storybook
```

## Deploy

**Backend**
<br>
BitBucket pipeline deploys the lambda function and sets up API gateway with Serverless

**Frontend**
<br>
Netlify watches BitBucket master branch and deploys whenever it detects a change in the branch


## Commit message conventions

- `feature: ...` For commits that introduce features 
- `fix: ...` For commits that apply bug fixes


## Todo

- Elegantly prevent setting state on unmounted components
- Implement reset password homepage handler
