import styled from 'styled-components'

export const Container = styled.span`
  display: inline-flex;
  font-size: 17px;
  color: rgba(0,0,0,0.8);
  &:hover {
    opacity: 0.5;
  }
  &:active {
    opacity: 1;
  }
`
