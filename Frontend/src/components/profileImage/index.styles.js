import styled from 'styled-components'

export const Image = styled.img`
    border-radius: 50%;
    cursor: pointer;
    padding: 5px;
    &:hover{
        background: rgba(0,0,0,0.08);
    }
`
