import { faPen } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import Component from './index'

const settings = {
  component: Component,
  title: 'IconButton'
}

export default settings
const Template = (args) => (
  <Component {...args}>
    <FontAwesomeIcon icon={faPen} />
  </Component>
)

export const Default = Template.bind({})
Default.args = {}
