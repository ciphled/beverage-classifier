import { useEffect, useState } from 'react'
import { Redirect, Route } from 'react-router-dom'
import { supabase } from '../../lib/supabase'

function ProtectedRoute ({ children, ...rest }) {
  const [loggedIn, setLoggedIn] = useState(!!supabase.auth.session())
  useEffect(() => {
    supabase.auth.onAuthStateChange((event, session) => {
      setLoggedIn(!!session)
    })
  }, [])
  return (
    <Route {...rest}>
      {
      loggedIn
        ? children
        : <Redirect to='/signin' />
      }
    </Route>
  )
}

export default ProtectedRoute
