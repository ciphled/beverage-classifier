const tf = require('@tensorflow/tfjs')
const fs = require('fs')
const path = require('path')

const MODEL_URL = 'https://beverage-classifier-model.s3-ap-southeast-2.amazonaws.com/model.json'
const WORD_DICT_PATH = path.join(__dirname, './word_dict.json')

async function loadModel () {
  const model = await tf.loadLayersModel(MODEL_URL)
  return model
}

async function loadTokeniser () {
  const tokenMapStr = fs.readFileSync(WORD_DICT_PATH)
  return await JSON.parse(tokenMapStr)
}

function standardise (s) {
  s = s.toLowerCase()
  s = s.replace(/ -.*$/g, '')
  s = s.replace(/ (the|or|and|\.|,|and|of|[1-9][0-9]*ml)/g, '')
  s = s.replace(/[.|\\|/|-| |,|!|;|:]/g, ' ')
  s = s.replace(/\s+/g, ' ')
  s = s.trim()
  if (s !== '') {
    return s
  }
  return '.'
}

async function getTokenisedText (text) {
  const stdText = standardise(text)
  const MAX_SEQUENCE_LENGTH = 50

  const wordToToken = await loadTokeniser()

  // Build sequence
  const parts = stdText.split(' ').slice(0, MAX_SEQUENCE_LENGTH)
  let seq = parts.map((word) => {
    const token = wordToToken[word]
    return token || 0
  })
  // Pad sequence
  const padding = new Array(MAX_SEQUENCE_LENGTH - seq.length)
  padding.fill(0)
  seq = padding.concat(seq)

  return tf.tensor1d(seq, 'int32').expandDims(0)
}

module.exports = {
  loadModel,
  loadTokeniser,
  standardise,
  getTokenisedText
}
