
function Divider ({ style }) {
  return (
    <hr style={{ margin: 0, opacity: 0.3, ...style }} />
  )
}

export default Divider
