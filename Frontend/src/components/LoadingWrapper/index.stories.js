import React from 'react'
import Component from './index'

const settings = {
  component: Component,
  title: 'LoadingWrapper'
}

export default settings
const Template = args => (
  <Component {...args}>
    <span style={{ background: 'red' }}>
      Test
    </span>
  </Component>

)

export const Default = Template.bind({})
Default.args = {
}
