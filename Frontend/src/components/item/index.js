import { faTrash } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { useState } from 'react'
import IconButton from '../iconButton'
import LoadingWrapper from '../LoadingWrapper'
import ModalDialog from '../modalDialog'
import OutsideClick from '../OutsideClick'
import { Header, Body, Container, ButtonsContainer, StyledInput, ContentContainer } from './index.styles'

/*
must incorporate ud somehow:
dropdown is overkill and not that aesthetic
 - pen-edit
    - edit the text directly, onBlur trigger update
 - trash-can delete
    - modal confirmation.
*/

const colorByType = {
  Wine: 'red',
  Liquor: 'rgb(0,100,255)',
  Beer: 'gold',
  Dunnage: 'rgb(200,0,255)',
  Kegs: 'silver',
  'Non-alcoholic': 'green'
}

function Item ({ text, id, type, conf, onDelete, onEdit, showConf }) {
  const [value, setValue] = useState(text)
  const [canEdit, setCanEdit] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const [showModal, setShowModal] = useState(false)

  const confText = ` with ${conf * 100}% confidence`
  const handleEditClick = () => {
    setCanEdit(true)
  }

  const handleTrashClick = () => {
    setShowModal(true)
  }

  const handleDelete = async () => {
    setShowModal(false)
    if (onDelete) {
      setIsLoading(true)
      await onDelete(id)
      setIsLoading(false)
    }
  }

  const handleChange = ({ target }) => {
    setValue(target.value)
  }

  const handleUpdate = async () => {
    setCanEdit(false)
    if (onEdit) {
      setIsLoading(true)
      await onEdit(id, value)
      setIsLoading(false)
    }
  }

  return (
    <Container>
      <ModalDialog
        text='Are you sure you want to delete this beverage?'
        header='Delete Beverage?'
        onCancel={() => setShowModal(false)}
        onConfirm={handleDelete}
        show={showModal}
      />
      <LoadingWrapper isLoading={isLoading} />
      <ContentContainer color={colorByType[type]}>
        <Header>
          <div>
            {type}
            {showConf && (
              <small style={{ fontWeight: 400 }}>
                {confText}
              </small>
            )}
          </div>
          <ButtonsContainer>
            <IconButton onClick={handleTrashClick}>
              <FontAwesomeIcon icon={faTrash} />
            </IconButton>
          </ButtonsContainer>
        </Header>
        <OutsideClick onOutsideClick={handleUpdate} disable={!canEdit}>
          <Body>
            <StyledInput
              type='text'
              onClick={handleEditClick}
              value={value}
              enabled={canEdit}
              onChange={handleChange}
            />
          </Body>
        </OutsideClick>
      </ContentContainer>
    </Container>
  )
}

export default Item
