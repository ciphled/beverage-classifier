import React from 'react'
import Component from './index'

const settings = {
  component: Component,
  title: 'Navbar'
}

export default settings
const Template = args => <Component {...args} />

export const Default = Template.bind({})
Default.args = {
}
