import styled from 'styled-components'
export const IconContainer = styled.div`
  display: inline-block;
  ${props => props.hidden && `
    display: none;
  `}
  &:hover{
    opacity: 0.6;
  };
  &:active{
    opacity: 1;
  };
  cursor: pointer;
  padding: 3px;
  ${props => props.color && `
      color: ${props.color}
    `
  }
`
