const { loadModel, getTokenisedText } = require('../util')

async function classify (event = {}) {
  try {
    const { body } = event
    if (!body) throw new Error('body missing')
    const parsedBody = JSON.parse(body)

    const { text } = parsedBody
    if (!text) throw new Error('text parameter missing')

    const classes = ['Beer', 'Dunnage', 'Kegs', 'Liquor', 'Non-Alcohol', 'Wine']
    const model = await loadModel()
    const tokenisedText = await getTokenisedText(text)
    const pred = await model.predict(tokenisedText).data()

    // pick class with the max conf
    let maxConfIdx = 0
    for (const idx in pred) {
      if (pred[maxConfIdx] < pred[idx]) {
        maxConfIdx = idx
      }
    }

    const prediction = {
      category: classes[maxConfIdx],
      conf: pred[maxConfIdx]
    }

    return {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(prediction)
    }
  } catch (e) {
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(
        {
          message: e.message,
          event,
          error: true
        },
        null,
        2
      )
    }
  }
}

module.exports = classify
