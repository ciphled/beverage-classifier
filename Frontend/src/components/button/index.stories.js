import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import Component from './index'

const settings = {
  component: Component,
  title: 'Button'
}

export default settings
const Template = (args) => (
  <Component {...args}>
    Add
  </Component>
)
export const Default = Template.bind({})
Default.args = {
  Icon: () => <FontAwesomeIcon icon={faPlus} />
}
