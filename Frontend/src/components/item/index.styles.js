import styled from 'styled-components'
export const Container = styled.div`
  width: 100%;
  position: relative;
`

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  font-weight: 600;
`

export const Body = styled.div`
  display: flex;
  align-items: center;
  padding: 10px 0;
`

export const ButtonsContainer = styled.div`
  display: grid;
  grid-auto-flow: column;
  gap: 5px;
`

export const StyledInput = styled.input`
  border: none;
  outline: none;
  width: 100%;
  font-size: 15px;
  &:disabled {
    background: none;
    opacity: 1;
    color: black;
  }
  cursor: text;
  padding: 5px;
  border-radius: 5px;
  &:hover{
    background: rgba(0,0,0,0.1);
  }
  ${props => props.enabled && `
    border: 2px solid rgba(0,150,255);
    &:hover{
      background: none;
    }
  `}
  
`

export const ContentContainer = styled.div`
  padding: 5px;
  position: relative;
  border: 1px solid grey;
  border-radius: 3px;
  ${props => props.color && `
    border-left: 5px solid ${props.color};
  `}
`
