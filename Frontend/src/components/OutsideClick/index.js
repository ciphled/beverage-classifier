import React, { useRef, useEffect } from 'react'

function useOutsideClick (ref, onOutsideClick, disable) {
  useEffect(() => {
    function handleClickOutside (event) {
      if (ref.current && !ref.current.contains(event.target)) {
        onOutsideClick()
      }
    }
    if (disable) {
      document.removeEventListener('click', handleClickOutside)
    } else {
      document.addEventListener('click', handleClickOutside)
    }
    return () => {
      document.removeEventListener('click', handleClickOutside)
    }
  }, [ref, onOutsideClick, disable])
}

export default function OutsideClick ({ onOutsideClick, children, disable, ...rest }) {
  const wrapperRef = useRef(null)
  useOutsideClick(wrapperRef, onOutsideClick, disable)
  return <div ref={wrapperRef} {...rest}><div>{children}</div></div>
}
