import { Container, Toggle } from './index.styles'

export default function Switch ({ onToggle, isOn = false, ...rest }) {
  const handleToggle = () => {
    if (onToggle) onToggle(isOn)
  }
  return (
    <Container isOn={isOn} onClick={handleToggle} {...(rest || {})}>
      <Toggle isOn={isOn} />
    </Container>
  )
}
