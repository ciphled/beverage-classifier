import styled from 'styled-components'

export const Container = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    padding: 10px;
`

export const ButtonContainer = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center; 
`
