import styled from 'styled-components'

export const FieldsContainer = styled.div`
    display: grid;
    width: 100%;
    gap: 20px;
    margin: 20px;
`
export const FormContainer = styled.div`
    display: flex;
    width: 100%;
    max-width: 800px;
    background: white;
    border: 1px solid lightgrey;
    border-radius: 5px;
    margin: 15px 0;
`
