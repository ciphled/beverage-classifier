import { IconContainer } from './index.styles'

function IconButton ({ children, onClick, color, hidden }) {
  const handleClick = () => {
    if (onClick) {
      onClick()
    }
  }

  return (
    <IconContainer hidden={hidden} onClick={handleClick} color={color || 'rgb(100,110,100)'}>
      {children}
    </IconContainer>
  )
}

export default IconButton
