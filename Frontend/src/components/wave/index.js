import { WaveContainer } from './index.styles'
import ReactDOM from 'react-dom'

function Wave () {
  const content = (
    <WaveContainer>
      <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 1440 320'>
        <path
          fill='#fff'
          d='M0,160L60,138.7C120,117,240,75,360,74.7C480,75,600,117,720,149.3C840,181,960,203,1080,181.3C1200,160,1320,96,1380,64L1440,32L1440,0L1380,0C1320,0,1200,0,1080,0C960,0,840,0,720,0C600,0,480,0,360,0C240,0,120,0,60,0L0,0Z'
        />
      </svg>
    </WaveContainer>
  )
  return ReactDOM.createPortal(content, document.getElementById('wave'))
}

export default Wave
