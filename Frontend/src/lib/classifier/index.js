import axios from 'axios'

/*
simple api calls to invoke AWS lambda classifier
*/

export async function classify (text) {
  try {
    const url = process.env.REACT_APP_CLASSIFIER
    const payload = { text }
    const response = await axios.post(url, payload)
    return response.data
  } catch (e) {
    console.error(e)
  }
}
