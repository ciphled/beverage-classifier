import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Container } from './index.styles'
export default function CloseButton ({ onClose }) {
  const handleClose = () => {
    if (onClose) onClose()
  }
  return (
    <Container onClick={handleClose}>
      <FontAwesomeIcon icon={faTimes} />
    </Container>
  )
}
