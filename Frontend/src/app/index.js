// import logo from './logo.svg'
import './App.css'
import { AppContainer } from './index.styles'
import Navbar from '../components/navbar'
import Routes from '../routes'

function App () {
  return (
    <AppContainer>
      <Navbar />
      <Routes />
    </AppContainer>
  )
}

export default App
