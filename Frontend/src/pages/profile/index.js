import { useEffect, useState } from 'react'
import Switch from '../../components/switch'
import Button from '../../components/button'
import Field from '../../components/field'
import Form from '../../components/form'
import { getAccount, supabase, updateProfile, updateSettings } from '../../lib/supabase'
import { Container, SettingsRow } from './index.styles'
import LoadingWrapper from '../../components/LoadingWrapper'

function SignUp () {
  const [name, setName] = useState('')
  const [showConf, setShowConf] = useState(false)
  const [nameIsLoading, setNameIsLoading] = useState(true)
  const [confIsLoading, setConfIsLoading] = useState(true)

  useEffect(() => {
    const updateAccount = async () => {
      const account = await getAccount()
      setName(account?.uid?.name)
      setShowConf(account?.showConf)
      setNameIsLoading(false)
      setConfIsLoading(false)
    }
    updateAccount()
    // const subscription = supabase
    //   .from('users')
    //   .on('*', updateAccount)
    //   .subscribe()
    // return () => {
    //   subscription?.unsubscribe()
    // }
  }, [])

  const handleNameChange = ({ target }) => {
    const newName = target.value
    setName(newName)
  }

  const handleToggle = async () => {
    const newShowConf = !showConf
    setShowConf(newShowConf)
    setConfIsLoading(true)
    await updateSettings({ showConf: newShowConf })
    setConfIsLoading(false)
  }

  const resetPassword = async (e) => {
    // Read more on https://supabase.io/docs/reference/javascript/reset-password-email#notes
    e.preventDefault()
    const email = window.prompt('Please enter your email:')
    const { error } = await supabase.auth.api.resetPasswordForEmail(email)
    if (error) {
      console.error('Error: ', error.message)
    }
  }

  const handleNameUpdate = async () => {
    setNameIsLoading(true)
    console.log('new', name)
    await updateProfile({ name })
    setNameIsLoading(false)
  }

  return (
    <Container>
      <h1>Account</h1>
      <Form>
        <h2 style={{ margin: 0 }}>Profile</h2>
        <Field
          title='name'
          type='text'
          value={name}
          onChange={handleNameChange}
          onBlur={handleNameUpdate}
          isLoading={nameIsLoading}
          required
        />
      </Form>
      <Form>
        <h2 style={{ margin: 0 }}>Settings</h2>
        <SettingsRow>
          Show class confidence level
          <Switch isOn={showConf} onToggle={handleToggle} />
          <LoadingWrapper isLoading={confIsLoading} />
        </SettingsRow>
        <div>
          <Button onClick={resetPassword} variant='link'>
            Reset Password?
          </Button>
        </div>
      </Form>
    </Container>

  )
}

export default SignUp
