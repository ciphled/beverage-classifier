import styled from 'styled-components'

export const ContentContainer = styled.div`
padding: 0 0 18px 0;
`

export const Container = styled.div`
  width: clamp(300px, 500px, 90%);
  background: white;
  border-radius: 5px;
  border: 1px solid grey;
  padding: 0 18px;
  margin: 5px;
`

export const ButtonsContainer = styled.div`
  display: grid;
  justify-content: flex-end;
  grid-auto-flow: column;
  gap: 5px;
  padding: 18px 0;
`

export const HeaderContainer = styled.div`
  font-weight: 700;
  font-size: 18px;
  padding: 10px 0;
`
