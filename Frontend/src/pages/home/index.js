import { useEffect, useState } from 'react'
import AddBar from '../../components/addBar'
import Item from '../../components/item'
import Spinner from '../../components/spinner'
import { classify } from '../../lib/classifier'
import { addItem, deleteItem, editItem, getAccount, getItems, supabase } from '../../lib/supabase'
import { Container, HomeContainer, HeaderContainer, ListContainer } from './index.styles'
/*
Get data
*/

function Home (props) {
  const [items, setItems] = useState([])
  const [isLoading, setIsLoading] = useState(true)
  const [showConf, setShowConf] = useState(true)

  useEffect(() => {
    async function updateItems (change) {
      const account = await getAccount()
      const items = await getItems()
      setShowConf(account?.showConf)
      setIsLoading(false)
      setItems(items)
    }
    updateItems()
    const subscription = supabase
      .from('beverages')
      .on('*', updateItems)
      .subscribe()
    return () => {
      subscription?.unsubscribe()
    }
  }, [])

  const handleAdd = async (text) => {
    const { category, conf } = await classify(text)
    await addItem(text, category, conf)
  }

  const handleDelete = async (id) => {
    await deleteItem(id)
  }

  const handleEdit = async (id, text) => {
    const { category, conf } = await classify(text)
    await editItem(id, text, category, conf)
  }

  return (
    <Container>
      <HomeContainer>
        <HeaderContainer>
          <AddBar onAdd={handleAdd} />
        </HeaderContainer>
        <ListContainer>
          {isLoading && <Spinner />}
          {items.map(item => {
            const {
              category: type,
              text,
              conf,
              id
            } = item
            return (
              <Item
                type={type}
                text={text}
                conf={conf}
                showConf={showConf}
                onDelete={handleDelete}
                onEdit={handleEdit}
                id={id}
                key={id}
              />
            )
          })}

        </ListContainer>
      </HomeContainer>
    </Container>
  )
}

export default Home
