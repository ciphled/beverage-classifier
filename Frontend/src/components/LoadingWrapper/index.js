import Spinner from '../spinner'
import { LoadingContainer } from './index.styles'

function LoadingWrapper ({ isLoading, size }) {
  return (
    <LoadingContainer isLoading={isLoading}>
      <Spinner size={size} />
    </LoadingContainer>
  )
}

export default LoadingWrapper
