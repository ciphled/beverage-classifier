import styled from 'styled-components'

export const HomeContainer = styled.div`
    display: grid;
    width: 100%;
    grid-template-rows: auto 1fr;
    grid-template-columns: 1fr;
    justify-content: center;
    gap: 30px;
    align-items: center; 
    max-width: 800px;
`

export const ListContainer = styled.div`
    display: flex; 
    align-items: center; 
    justify-content: center;
    flex-direction: column;
    gap: 10px;
`

export const HeaderContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
`

export const Container = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    flex-direction: column;
    padding: 20px;
`
