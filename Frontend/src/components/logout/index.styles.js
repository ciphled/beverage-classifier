import styled from 'styled-components'

export const Title = styled.a`
    margin: auto;
    cursor: pointer;
    color: rgba(0,150,255, 1);
    font-size: 18px;
    &:hover {
        text-decoration: underline;
    }
`
