import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { useState } from 'react'
import Button from '../button'
import Field from '../field'

export default function AddBar ({ onAdd }) {
  const [value, setValue] = useState('')
  const [isLoading, setIsLoading] = useState(false)
  const PlusIcon = () => (
    <FontAwesomeIcon icon={faPlus} />
  )
  const handleAdd = async () => {
    if (onAdd) {
      setIsLoading(true)
      await onAdd(value)
      setIsLoading(false)
    }
    setValue('')
  }
  const handleChange = ({ target }) => {
    const value = target.value
    setValue(value)
  }
  return (
    <Field title='Beverage' value={value} onChange={handleChange}>
      <Button onClick={handleAdd} Icon={PlusIcon} style={{ margin: 8 }} isLoading={isLoading}>
        Add
      </Button>
    </Field>
  )
}
