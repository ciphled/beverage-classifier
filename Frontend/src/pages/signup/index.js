import { useEffect, useState } from 'react'
import { useHistory } from 'react-router'
import Button from '../../components/button'
import Field from '../../components/field'
import Form from '../../components/form'
import { addUser, supabase } from '../../lib/supabase'
import { Container, ButtonContainer } from './index.styles'

function SignUp () {
  const [helperText, setHelperText] = useState({ error: null, text: null })
  const [email, setEmail] = useState()
  const [password, setPassword] = useState()
  const [name, setName] = useState()
  const history = useHistory()

  useEffect(() => {
    if (supabase.auth.session()) {
      history.replace('/')
    }
  }, [history])

  const handleSignin = async () => {
    history.push('signin')
  }

  const handleSignup = async () => {
    // const email = emailRef.current?.value
    // const password = passwordRef.current?.value
    const { user, error } = await supabase.auth.signUp({ email, password })
    await addUser(user.id, name)
    if (error) {
      setHelperText({ error: true, text: error.message })
    }
    setHelperText({
      error: false,
      text: 'An email has been sent to you for verification!'
    })
  }

  const handleEmailChange = ({ target }) => {
    setEmail(target.value)
  }

  const handlePasswordChange = ({ target }) => {
    setPassword(target.value)
  }

  const handleNameChange = ({ target }) => {
    setName(target.value)
  }

  // const handleOAuthLogin = async (provider) => {
  //   // You need to enable the third party auth you want in Authentication > Settings
  //   // Read more on: https://supabase.io/docs/guides/auth#third-party-logins
  //   const { error } = await supabase.auth.signIn({ provider })
  //   if (error) console.log('Error: ', error.message)
  // }

  const forgotPassword = async (e) => {
    // Read more on https://supabase.io/docs/reference/javascript/reset-password-email#notes
    e.preventDefault()
    const email = window.prompt('Please enter your email:')

    if (email === null || email === '') {
      setHelperText({ error: true, text: 'You must enter your email.' })
    } else {
      const { error } = await supabase.auth.api.resetPasswordForEmail(email)
      if (error) {
        console.error('Error: ', error.message)
      } else {
        setHelperText({
          error: false,
          text: 'Password recovery email has been sent.'
        })
      }
    }
  }

  return (
    <Container>
      <h1>Sign Up</h1>
      <Form>
        <Field
          title='name'
          type='text'
          onChange={handleNameChange}
          required
        />
        <Field
          title='email'
          type='text'
          onChange={handleEmailChange}
          required
        />
        <Field
          title='password'
          type='password'
          onChange={handlePasswordChange}
          required
        />
        {!!helperText.text && <div>{helperText.text}</div>}
        <div>
          <Button onClick={forgotPassword} variant='link'>
            Forgot Password?
          </Button>
        </div>
        <br />
        <br />

        <ButtonContainer>
          <Button onClick={handleSignin} variant='link'>
            Have an account?
          </Button>
          <Button onClick={handleSignup}>Sign Up</Button>
        </ButtonContainer>
      </Form>

    </Container>
  )
}

export default SignUp
