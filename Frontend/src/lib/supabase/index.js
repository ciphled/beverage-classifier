import { createClient } from '@supabase/supabase-js'
import { key, url } from './constants'

export const supabase = createClient(url, key)

// need the class
export async function addItem (text, category, conf) {
  const uid = supabase.auth.user().id
  const { data, error } = await supabase.from('beverages').insert(
    {
      text,
      category,
      conf,
      uid
    },
    {
      returning: 'minimal'
    }
  )
  if (error) {
    console.error(error)
  }
  return data
}

export async function getItems () {
  const uid = supabase.auth.user().id
  const { data, error } = await supabase
    .from('beverages')
    .select('*')
    .match({
      uid
    })
    .order('createdAt', { ascending: false })
  if (error) {
    console.error(error)
  }
  return data
}

export async function deleteItem (id) {
  const { data, error } = await supabase
    .from('beverages')
    .delete({ returning: 'minimal' })
    .match({
      id
    })
  if (error) {
    console.error(error)
  }
  return data
}

export async function addUser (id, name, avatar = '') {
  const { data, error } = await supabase
    .from('users')
    .insert(
      {
        id,
        name,
        avatar
      },
      {
        returning: 'minimal'
      }
    )
  const { userError } = await supabase
    .from('user_settings')
    .insert(
      {
        uid: id,
        showConf: true
      },
      {
        returning: 'minimal'
      }
    )
  if (error) {
    console.error(error)
  }
  if (userError) {
    console.error(userError)
  }
  return data
}

export async function editItem (id, text, category, conf) {
  const { data, error } = await supabase
    .from('beverages')
    .update(
      {
        text,
        category,
        conf
      },
      {
        returning: 'minimal'
      }
    )
    .match({
      id
    })
  if (error) {
    console.error(error)
  }
  return data
}

export async function getAccount () {
  // retrieves user profile including their settings
  const uid = supabase.auth.user().id

  const { data, error } = await supabase
    .from('user_settings')
    .select(`
      *,
      uid (
        *
      )
    `)
    .match({
      uid
    })
  if (error) {
    console.error(error)
  }
  return data.pop()
}

export async function updateSettings ({ showConf }) {
  if (showConf === undefined) return

  const uid = supabase.auth.user().id

  const { data, error } = await supabase
    .from('user_settings')
    .update(
      {
        showConf
      },
      {
        returning: 'minimal'
      }
    )
    .match({
      uid
    })
  if (error) {
    console.error(error)
  }
  return data
}

export async function updateProfile ({ name }) {
  if (!name) return

  const id = supabase.auth.user().id
  const { data, error } = await supabase
    .from('users')
    .update(
      {
        name
      },
      {
        returning: 'minimal'
      }
    )
    .match({
      id
    })
  if (error) {
    console.error(error)
  }
  return data
}
