import React from 'react'
import { Switch, Route } from 'react-router-dom'
import * as ROUTES from './constants'

import { Profile, SignIn, SignUp, ResetPassword, Home } from '../pages'
import ProtectedRoute from '../components/protectedRoute'

export default function Routes (props) {
  return (
    <Switch>
      <ProtectedRoute exact path={ROUTES.profile}>
        <Profile />
      </ProtectedRoute>
      <Route exact path={ROUTES.signin}>
        <SignIn />
      </Route>
      <Route exact path={ROUTES.signup}>
        <SignUp />
      </Route>
      <Route exact path={ROUTES.resetpw}>
        <ResetPassword />
      </Route>
      <ProtectedRoute exact path={ROUTES.home}>
        <Home />
      </ProtectedRoute>
    </Switch>
  )
}
