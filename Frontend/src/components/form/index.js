import { FormContainer, FieldsContainer } from './index.styles'

function Form ({ children }) {
  return (
    <FormContainer>
      <FieldsContainer>
        {children}
      </FieldsContainer>
    </FormContainer>
  )
}

export default Form
